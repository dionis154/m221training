<?php

namespace Training\Test\Block\Training;

class ContentTwo extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }


    public function anyMethod()
    {
        return 'some content two...';
    }
}

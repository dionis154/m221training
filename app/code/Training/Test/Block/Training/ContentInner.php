<?php

namespace Training\Test\Block\Training;

class ContentInner extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }


    public function anyMethod()
    {
        return 'some INNER content one...';
    }

    public function getSomeStringValue()
    {
        return $this->getSomeString();
    }

    public function getSomeArrayValue()
    {
        return $this->getSomeArray() ?: [];
    }

}

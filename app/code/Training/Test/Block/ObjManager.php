<?php
namespace Training\Test\Block;

use Magento\Framework\View\Element\Template;

class ObjManager extends \Magento\Framework\View\Element\Template
{
    protected $customerSession;

    protected $newProducts;

    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\Rss\Product\NewProducts $newProducts,
        array $data
    ) {
        $this->customerSession = $customerSession;
        $this->newProducts = $newProducts;
        parent::__construct($context, $data);
    }

    public function anyMethod()
    {
        $objManager = \Magento\Framework\App\ObjectManager::getInstance();
        $anyClass = $objManager->get('Magento\Catalog\Model\Rss\Product\NewProducts');

        $this->newProducts;

    }


}
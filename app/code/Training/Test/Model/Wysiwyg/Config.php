<?php
namespace Training\Test\Model\Wysiwyg;

use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Ui\Component\Wysiwyg\ConfigInterface;

/**
 * Wysiwyg Config for Editor HTML Element
 *
 * @api
 * @since 100.0.2
 */
class Config extends \Magento\Cms\Model\Wysiwyg\Config
{
    public function __construct(
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\AuthorizationInterface $authorization,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Variable\Model\Variable\Config $variableConfig,
        \Magento\Widget\Model\Widget\Config $widgetConfig,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Filesystem $filesystem,
        array $windowSize,
        array $data
    ) {
        $windowSize = ['height' => 222, 'width' => 444];
        parent::__construct($backendUrl, $eventManager, $authorization, $assetRepo, $variableConfig, $widgetConfig, $scopeConfig, $storeManager, $filesystem, $windowSize, $data);
    }


}

<?php
namespace Training\Test\Observer;

use Magento\Framework\Event\ObserverInterface;

class CustomerRegisterSuccessObserver implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customer   = $observer->getData('account_controller');
        $controller = $observer->getAccountController();
        return $this;
    }
}

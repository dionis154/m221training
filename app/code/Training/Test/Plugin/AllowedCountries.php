<?php
namespace Training\Test\Plugin;
use Magento\Store\Model\ScopeInterface;
class AllowedCountries
{
    public function beforeGetAllowedCountries(
        \Magento\Directory\Model\AllowedCountries $subject,
        $scope = ScopeInterface::SCOPE_WEBSITE,
        $scopeCode = null
    ) {
        //any logic
        return [$scope, $scopeCode];
    }

    public function afterGetAllowedCountries(
        \Magento\Directory\Model\AllowedCountries $subject,
        $result
    ) {
        //any logic
        return $result;
    }

    public function aroundGetAllowedCountries(
        \Magento\Directory\Model\AllowedCountries $subject,
        \Closure $proceed,
        $scope = ScopeInterface::SCOPE_WEBSITE,
        $scopeCode = null
    ) {
        //any logic before
        $result = $proceed($scope, $scopeCode);
        //any logic after
        return [$scope, $scopeCode];
    }
}
